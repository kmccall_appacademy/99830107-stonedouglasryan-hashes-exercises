# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_list = Hash.new { 0 }
  words = str.split

  words.each do |word|
    word_list[word] = word.length
  end
  word_list
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |k, v| v }[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k, v|
    older[k] = v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  counter_hash = Hash.new { 0 }
  letters = word.split("")

  letters.each do |letter|
    counter_hash[letter] += 1
  end
  counter_hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  count = {}
  arr.each { |ele| count[ele] = true }
  count.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  count = {
    even: 0,
    odd: 0
  }

  numbers.each do |ele|
    if ele.odd?
      count[:odd] += 1
    elsif ele.even?
      count[:even] += 1
    end
  end
  count
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  letter_count = letter_counts(string)
  letter_count.select! { |k, v| "aeiou".include?(k) }

  vowel_arr = letter_count.sort_by { |k, v| v }
  vowel_arr[-1][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  winter_students = students.select { |student, month| month >= 7 }

  names = winter_students.keys
  return_array = []

  names.each_index do |idx|
    ((idx + 1)...names.length).each do |i|
      return_array << [names[idx], names[i]]
    end
  end
  return_array
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  uinq_arr = specimens.uniq
  species = {}

  uinq_arr.each do |ele|
    species[ele] = specimens.count(ele)
  end

  species_amount = uinq_arr.length
  smallest_species = species.values.min
  largest_species = species.values.max

  species_amount ** 2 * smallest_species / largest_species
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_amount = character_count(normal_sign)
  vandal_amount = character_count(vandalized_sign)

  vandal_amount.all? do |letter, count|
    normal_amount[letter.downcase] >= count
  end
end

def character_count(str)
  count = Hash.new(0)

  str.each_char do |letter|
    count[letter.downcase] += 1 unless letter == " "
  end
  count
end
